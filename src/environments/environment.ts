export const environment = {
  production: false,
  twitchClientId: 'gvdb3exxtebeuhblxseqn6lf0rwo7i',
  twitchApiUrl: 'https://api.twitch.tv/helix',
  videoPlayerUrl: 'http://player.twitch.tv?channel=[USERNAME]',
  chatRoomUrl: 'https://www.twitch.tv/embed/[USERNAME]/chat',
};
