import {Component, Input} from '@angular/core';
import {VideoModel} from '../../models/video.model';

@Component({
  selector: 'app-video-card',
  templateUrl: './video-card.component.html',
  styleUrls: ['./video-card.component.scss']
})
export class VideoCardComponent {
  @Input() video: VideoModel;
}
