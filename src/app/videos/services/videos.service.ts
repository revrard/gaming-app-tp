import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {VideoModel} from '../models/video.model';
import {map, tap} from 'rxjs/operators';
import {TwitchApiResponseModel} from '../../api/models/twitch-api-response.model';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VideosService {

  constructor(private http: HttpClient) {
  }

  getVideosByGameId(gameId: number): Observable<VideoModel[]> {
    return this.getTwitchVideosByGameId(gameId).pipe(
      map(twitchApiResponse => twitchApiResponse.data),
    ) as Observable<VideoModel[]>;
  }

  private getTwitchVideosByGameId(gameId: number) {
    return this.http.get<TwitchApiResponseModel>(`${environment.twitchApiUrl}/videos?game_id=${gameId}`);
  }
}
