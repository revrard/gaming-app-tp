import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ClipModel} from '../models/clip.model';
import {map} from 'rxjs/operators';
import {TwitchApiResponseModel} from '../../api/models/twitch-api-response.model';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClipsService {

}
