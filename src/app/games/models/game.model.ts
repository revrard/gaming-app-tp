export interface GameModel {
  id: string;
  name: string;
  box_art_url: string;
}
