import {Component, OnInit} from '@angular/core';
import {StreamsService} from '../../../streams/services/streams.service';
import {Observable} from 'rxjs';
import {StreamModel} from '../../../streams/models/stream.model';
import {ActivatedRoute} from '@angular/router';
import {map, switchMap} from 'rxjs/operators';
import {GameModel} from '../../models/game.model';
import {GamesService} from '../../services/games.service';

@Component({
  selector: 'app-game-live-channels',
  templateUrl: './game-live-channels.component.html',
  styleUrls: ['./game-live-channels.component.scss']
})
export class GameLiveChannelsComponent implements OnInit {

  streams$: Observable<StreamModel[]>;
  game$: Observable<GameModel>;

  constructor(private activatedRoute: ActivatedRoute, private streamsService: StreamsService, private gamesService: GamesService) {
  }

  ngOnInit(): void {
    this.streams$ = this.activatedRoute.params.pipe(
      switchMap(params => this.streamsService.getStreamsByGameId(params.gameId))
    );
    this.game$ = this.activatedRoute.params.pipe(
      switchMap(params => this.gamesService.getGameById(params.gameId))
    );
  }
}
