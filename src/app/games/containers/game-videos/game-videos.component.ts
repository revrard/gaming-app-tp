import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {StreamModel} from '../../../streams/models/stream.model';
import {GameModel} from '../../models/game.model';
import {ActivatedRoute} from '@angular/router';
import {StreamsService} from '../../../streams/services/streams.service';
import {GamesService} from '../../services/games.service';
import {switchMap} from 'rxjs/operators';
import {VideoModel} from '../../../videos/models/video.model';
import {VideosService} from '../../../videos/services/videos.service';

@Component({
  selector: 'app-game-videos',
  templateUrl: './game-videos.component.html',
  styleUrls: ['./game-videos.component.scss']
})
export class GameVideosComponent {


}
