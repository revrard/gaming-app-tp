import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../environments/environment';
import {TwitchApiResponseModel} from '../../api/models/twitch-api-response.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GameModel} from '../models/game.model';

@Injectable({
  providedIn: 'root'
})
export class GamesService {

  constructor(private http: HttpClient) {
  }

  getTopGames(): Observable<GameModel[]> {
    return this.http.get<TwitchApiResponseModel>(`${environment.twitchApiUrl}/games/top`).pipe(
      map(twitchApiResponse => twitchApiResponse.data),
    ) as Observable<GameModel[]>;
  }

  getGameById(gameId: number): Observable<GameModel> {
    return this.http.get<TwitchApiResponseModel>(`${environment.twitchApiUrl}/games?id=${gameId}`).pipe(
      map(twitchApiResponse => twitchApiResponse.data[0]),
    ) as Observable<GameModel>;
  }
}
