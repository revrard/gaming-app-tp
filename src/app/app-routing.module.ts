import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'games'},
  {path: 'games', loadChildren: () => import('./games/games.module').then(m => m.GamesModule)},
  {path: 'streams', loadChildren: () => import('./streams/streams.module').then(m => m.StreamsModule)},
  {path: '**', pathMatch: 'full', redirectTo: 'games'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
