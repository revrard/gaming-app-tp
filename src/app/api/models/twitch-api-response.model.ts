import {GameModel} from '../../games/models/game.model';
import {ClipModel} from '../../clips/models/clip.model';
import {StreamModel} from '../../streams/models/stream.model';
import {VideoModel} from '../../videos/models/video.model';

export interface TwitchApiResponseModel {
  data: [GameModel | ClipModel | StreamModel | VideoModel];
  pagination: { cursor: string };
}
