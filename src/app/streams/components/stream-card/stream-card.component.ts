import {Component, Input} from '@angular/core';
import {StreamModel} from '../../models/stream.model';

@Component({
  selector: 'app-stream-card',
  templateUrl: './stream-card.component.html',
  styleUrls: ['./stream-card.component.scss']
})
export class StreamCardComponent {
  @Input() stream: StreamModel;
}
