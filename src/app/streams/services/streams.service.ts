import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {TwitchApiResponseModel} from '../../api/models/twitch-api-response.model';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {StreamModel} from '../models/stream.model';

@Injectable({
  providedIn: 'root'
})
export class StreamsService {

  constructor(private http: HttpClient) {
  }

  getStreams(limit: number): Observable<StreamModel[]> {
    return this.http.get<TwitchApiResponseModel>(`${environment.twitchApiUrl}/streams?first=${limit}`).pipe(
      map(twitchApiResponse => twitchApiResponse.data),
    ) as Observable<StreamModel[]>;
  }

  getStreamByUserLogin(userLogin: string): Observable<StreamModel> {
    return this.http.get<TwitchApiResponseModel>(`${environment.twitchApiUrl}/streams?user_login=${userLogin}`).pipe(
      map(twitchApiResponse => twitchApiResponse.data[0]),
    ) as Observable<StreamModel>;
  }

  getStreamsByGameId(gameId: number): Observable<StreamModel[]> {
    return this.http.get<TwitchApiResponseModel>(`${environment.twitchApiUrl}/streams?game_id=${gameId}`).pipe(
      map(twitchApiResponse => twitchApiResponse.data),
    ) as Observable<StreamModel[]>;
  }
}
