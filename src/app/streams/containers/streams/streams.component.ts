import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {StreamModel} from '../../models/stream.model';
import {StreamsService} from '../../services/streams.service';

@Component({
  selector: 'app-streams',
  templateUrl: './streams.component.html',
  styleUrls: ['./streams.component.scss']
})
export class StreamsComponent {

}
