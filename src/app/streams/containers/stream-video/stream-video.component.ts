import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {StreamModel} from '../../models/stream.model';
import {ActivatedRoute} from '@angular/router';
import {StreamsService} from '../../services/streams.service';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-stream-video',
  templateUrl: './stream-video.component.html',
  styleUrls: ['./stream-video.component.scss']
})
export class StreamVideoComponent implements OnInit {

  stream$: Observable<StreamModel>;

  constructor(private activatedRoute: ActivatedRoute, private streamsService: StreamsService) {
  }

  ngOnInit(): void {
    this.stream$ = this.activatedRoute.params.pipe(
      switchMap(params => this.streamsService.getStreamByUserLogin(params.userLogin))
    );
  }

}
